# ParaView Plugin for ParFlow Support

To build the plugin,

```sh
git clone https://gitlab.kitware.com/parflow/paraview-plugin.git
mkdir build
cd build
cmake -G Ninja \
  -DParaView_DIR:PATH=/path/to/paraview \
  -DQt5_DIR:PATH=/path/to/qt \
  ..
ninja
```

Then run ParaView and load the plugin in the plugin manager.
You should then be able to load ParFlow's PFB output files.
